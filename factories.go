package edispatcher_rabbitmq

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

// NewEventChannel реализует фабрику канала доставки сообщений
func NewEventChannel(
	credentials string,
	exchange ExchangeParams,
	queueParams QueueParams,
) (channel edispatcher.EventChannelInterface, manager edispatcher.BusEventManagerInterface, err error) {
	connection, err := amqp.Dial(credentials)
	if nil != err {
		return
	}

	channel = &eventChannel{
		logger:     logrus.WithField("prefix", "edispatcher-rabbitmq/EventChannel"),
		connection: connection,
		exchange:   exchange,
		queue:      queueParams,
	}

	manager = &busEventManager{
		logger: logrus.WithField("prefix", "edispatcher-rabbitmq/BusEventManager"),
	}

	return
}
