package edispatcher_rabbitmq

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

// QueueParams содержит параметры очереди для RabbitMQ
type QueueParams struct {
	Name       string
	Exclusive  bool
	AutoDelete bool
	Durable    bool
	NoWait     bool
	Policies   map[string]interface{}
}

// ExchangeParams содержит параметры канала для RabbitMQ
type ExchangeParams struct {
	Name       string
	Kind       string
	Durable    bool
	AutoDelete bool
	Internal   bool
	NoWait     bool
	Policies   map[string]interface{}
}

// Канал для работы с сообщениями. Реализует интерфейс взаимодействия с RabbitMQ
type eventChannel struct {
	logger     *logrus.Entry
	connection *amqp.Connection
	exchange   ExchangeParams
	queue      QueueParams
}

// Dispatch выполняет доставку событий в шину
func (e eventChannel) Dispatch(ctx context.Context, eventsData []string) (err error) {
	if 0 == len(eventsData) {
		return nil
	}

	e.logger.WithFields(logrus.Fields{
		"code":   100,
		"events": eventsData,
	}).Info("Sending events to RabbitMQ")

	channel, err := e.connection.Channel()
	if nil != err {
		err = errors.Wrap(err, "failed to open channel in RabbitMQ")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to open channel in RabbitMQ`)

		return
	}

	err = channel.ExchangeDeclare(
		e.exchange.Name,
		e.exchange.Kind,
		e.exchange.Durable,
		e.exchange.AutoDelete,
		e.exchange.Internal,
		e.exchange.NoWait,
		e.exchange.Policies,
	)
	if nil != err {
		err = errors.Wrap(err, "failed to create exchange in RabbitMQ")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to create exchange in RabbitMQ`)

		return
	}

	defer channel.Close()

	for _, event := range eventsData {
		err = channel.PublishWithContext(ctx, e.exchange.Name, "#", false, false, amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(event),
			MessageId:    uuid.New().String(),
		})

		if nil != err {
			err = errors.Wrap(err, "failed to dispatch event in RabbitMQ")

			e.logger.
				WithError(err).
				WithField("code", 500).
				Error(`Failed to dispatch event in RabbitMQ`)

			return
		}

		e.logger.WithFields(logrus.Fields{
			"code":     200,
			"exchange": e.exchange.Name,
			"taskBody": event,
		}).Info(`Event dispatched to RabbitMQ`)
	}

	return nil
}

// Receive выполняет получение событий из шины
func (e eventChannel) Receive(ctx context.Context, _ string, events chan *edispatcher.EventTemporaryData) (err error) {
	if nil == ctx {
		ctx = context.Background()
	}

	channel, err := e.connection.Channel()
	if nil != err {
		err = errors.Wrap(err, "failed to open channel in RabbitMQ")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to open channel in RabbitMQ`)

		return
	}

	defer channel.Close()

	err = channel.ExchangeDeclare(
		e.exchange.Name,
		e.exchange.Kind,
		e.exchange.Durable,
		e.exchange.AutoDelete,
		e.exchange.Internal,
		e.exchange.NoWait,
		e.exchange.Policies,
	)
	if nil != err {
		err = errors.Wrap(err, "failed create exchange in RabbitMQ")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to create exchange in RabbitMQ`)

		return
	}

	queue, err := channel.QueueDeclare(
		e.queue.Name,
		e.queue.Durable,
		e.queue.AutoDelete,
		e.queue.Exclusive,
		e.queue.NoWait,
		e.queue.Policies,
	)

	if nil != err {
		err = errors.Wrap(err, "failed to create queue in RabbitMQ")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to create queue in RabbitMQ`)

		return
	}

	err = channel.Qos(1, 0, false)
	if nil != err {
		err = errors.Wrap(err, "failed to configure QoS")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Can't configure QoS`)

		return
	}

	err = channel.QueueBind(queue.Name, "#", e.exchange.Name, false, nil)
	if nil != err {
		err = errors.Wrap(err, "can't bind queue to exchange")

		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Can't bind queue to exchange`)

		return
	}

	messageChan, err := channel.Consume(
		queue.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)

	for {
		select {
		case <-ctx.Done():
			return nil
		case message := <-messageChan:
			e.logger.WithFields(logrus.Fields{
				"code":    30001,
				"message": string(message.Body),
				"offset":  message.MessageId,
			}).Debug("Received message from RabbitMQ")

			events <- &edispatcher.EventTemporaryData{
				Event: edispatcher.StoredEventData{
					Id:   message.MessageId,
					Data: string(message.Body),
				},
				Origin: message,
			}
		}
	}
}
