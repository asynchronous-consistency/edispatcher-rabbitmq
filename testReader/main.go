package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-rabbitmq/v3"
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	log "github.com/sirupsen/logrus"
	"time"
)

func main() {
	ctx := context.Background()
	channel, manager, err := edispatcher_rabbitmq.NewEventChannel(
		"amqp://rabbit:rabbit@localhost:5672/",
		edispatcher_rabbitmq.ExchangeParams{
			Name:       "events",
			Kind:       amqp.ExchangeFanout,
			Durable:    true,
			AutoDelete: false,
			Internal:   false,
			NoWait:     false,
			Policies:   nil,
		},
		edispatcher_rabbitmq.QueueParams{
			Name:       fmt.Sprintf("events_%v", time.Now().Unix()),
			Exclusive:  true,
			AutoDelete: false,
			Durable:    true,
			NoWait:     false,
			Policies: map[string]interface{}{
				"x-ha-policy": "all",
			},
		},
	)

	if nil != err {
		log.WithError(err).Fatal(`Failed to create channel`)
	}

	messages := make(chan *edispatcher.EventTemporaryData, 50)
	go func() {
		log.WithError(channel.Receive(context.Background(), "-1", messages)).Fatal(`Failed to read messages`)
	}()

	for {
		msg := <-messages
		err := manager.CommitEvent(ctx, msg)
		if nil != err {
			log.WithError(err).Fatal(`Failed to commit message`)
		}

		log.Println(msg.Event)
	}
}
