package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-rabbitmq/v3"
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	log "github.com/sirupsen/logrus"
)

func main() {
	channel, _, err := edispatcher_rabbitmq.NewEventChannel(
		"amqp://rabbit:rabbit@localhost:5672/",
		edispatcher_rabbitmq.ExchangeParams{
			Name:       "events",
			Kind:       amqp.ExchangeFanout,
			Durable:    true,
			AutoDelete: false,
			Internal:   false,
			NoWait:     false,
			Policies:   nil,
		},
		edispatcher_rabbitmq.QueueParams{
			Exclusive:  false,
			AutoDelete: false,
			Durable:    true,
			NoWait:     false,
			Policies: map[string]interface{}{
				"x-ha-policy": "all",
			},
		},
	)

	if nil != err {
		log.WithError(err).Fatal(``)
	}

	ctx := context.Background()
	messages := make([]string, 200)
	for i := 0; i < 200; i++ {
		messages[i] = fmt.Sprintf(`%v`, i)
	}

	for {
		err = channel.Dispatch(ctx, messages)
		if nil != err {
			log.WithError(err).Fatal(`Failed to dispatch messages`)
		}
	}
}
