package edispatcher_rabbitmq

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

// Сервис для работы с событиями на стороне шины
type busEventManager struct {
	logger *logrus.Entry
}

// CommitEvent подтверждает принятие события на стороне шины
func (b busEventManager) CommitEvent(_ context.Context, event *edispatcher.EventTemporaryData) error {
	msg := event.Origin.(amqp.Delivery)
	err := msg.Ack(false)

	if nil != err {
		b.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to ACK message`)
	}

	return errors.Wrap(err, "failed to ACK message")
}

// RejectEvent отклоняет событие на стороне шины
func (b busEventManager) RejectEvent(_ context.Context, event *edispatcher.EventTemporaryData) error {
	msg := event.Origin.(amqp.Delivery)
	err := msg.Nack(false, true)

	if nil != err {
		b.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to NACK message`)
	}

	return errors.Wrap(err, "failed to NACK message")
}
